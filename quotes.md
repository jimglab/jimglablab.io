---
layout: page 
title: Quotes
---

Some quotes to keep in mind.

---
> To me, it is not a matter of whether it can be done, or even how to do it. It is a matter of whether it is likely that we would gain something from that exercise.
> -anonymous.


---
> Sweat today so you don't bleed tomorrow.
> -some martial artist.


---
> Study hard what interests you the most in the most undisciplined, irreverent and original manner possible.
> -Richard Feynmann


---
> The first principle is that you must not fool yourself and you are the easiest person to fool.
> -Richard Feynmann


---
> We never care about security until something happens.
> -Anonymous


---
> Everything we do to achieve privacy and security in the computer age depends on random numbers.
> -Simon Cooper


<!--- EOF -->
