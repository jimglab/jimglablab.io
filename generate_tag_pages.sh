#!/bin/bash

ftmp=tag/._tmp
# get all tags used in _posts/*.md
ss="$(grep tags _posts/*.md | awk '{s=""; for(i=2;i<=NF;i++) s=s $i " "; print s}')"
echo $ss > $ftmp
# unique set of tags
ssu=$(grep -o -E '[_[:alnum:]\-]+' $ftmp | sort -u -f)
rm $ftmp

# remove all tags
echo -e "\n [*] removing current tag pages"
rm -vf tag/*.md

# generate a .md page for each tag
echo -e "\n [*] generating tags pages:"
for su in ${ssu[@]}; do
fout=tag/$su.md
echo " > $fout"
cat <<EOF > $fout
---
layout: tag_index
tag: $su
---
EOF
done

echo ""
#EOF
