---
layout: page 
title: About
---

<!-------------------------------------
  ----------  S P E E C H  ------------
-------------------------------------->

# About me

I'm a Physics scientist (PhD), data science practitioner and a Linux explorer.
Right now I work as Data Scientist in the industry.
In physics, my work mainly involved data analysis and simulations related to [space weather](https://en.wikipedia.org/wiki/Space_weather): mainly, the "weather" conditions in the interplanetary environment that lies between the Sun and the Earth.
Specifically, I used to do data analysis of spacecraft _in situ_ measurements, which gives us information about the state of the interplanetary environment, and we can filter such data to study specific "events" of interest to conclude some physics about the dynamics of the plasma (_i.e._ the [solar wind](https://en.wikipedia.org/wiki/Solar_wind)) in such regions.
During my Postdoc research, I was also working on simulations related studies of particle acceleration in such scenarios.

On the other side (in background mode), I'm a Linux enthusiast.
I like to explore the freedom of choices it gives us, to entertain my creativity, to undestand computers, the evolution of software development, and because I like seeing ideas in code.
For the last two years, I've been using the distro [Arch Linux](https://wiki.archlinux.org), which has very little _ab initio_ configuration, in comparison with most Linux distributions.
This gave me a "push" to explore the pipelines of configurations available to the user.
It can be a pain, but it's worth it.
As Arch Linux is a "rolling" distribution (_i.e._ there are no "versions", but only plain releases), I must keep at the bleeding edge of software development if I want a dynamic maintainance of my box.
But as a scientist, I care much about reproducible work and for relatively "static" development environments.

This is where the "[Docker](https://www.docker.com)" part of my life comes in.
This project becomes interesting to me since it offers the bleeding edge tech on security, Linux container-related implementations, and it offers the right scenario for reproducible work!
In computational physics (_i.e._ simulation and data science), we usually are not worried about hardware emulation (e.g. network interfaces, hard disk, or the whole OS kernel).
Instead, we care about compiling code, linking dynamic library dependencies, and software versions in order to run custom code that we develop/get.
That is, "process isolation" is all we care about most of the time, and the most common solutions we come up with is creating virtual machines (VMs).
The advantage of Docker, over VirtuaBox, is that it doesn't have the overhead of the [Hypervisor](https://en.wikipedia.org/wiki/Hypervisor) (the layer that virtualizes the hardware)...
This is and advantage because it means our codes can run faster since it's talking directly to the host kernel.

In this context, I find [Arch Linux](https://wiki.archlinux.org) a perfect home for a Docker installation since we can constantly keep up with its development without the need of periodically re-installing the whole operative system, or sparce random libraries on the "real" host.



<!-------------------------------------
  ------  P U B L I C A T I O N S -----
-------------------------------------->
---

# Publications

My main scientific publications are listed below.
For a more complete list, please visit my [Google Scholar](https://scholar.google.com/citations?hl=es&user=ggw6ObwAAAAJ).

* [_"Superposed epoch study of ICME sub-structures near Earth and their effects on galactic cosmic rays"_](https://www.aanda.org/articles/aa/abs/2016/08/aa28571-16/aa28571-16.html), Masías-Meza J.J., Dasso S., Demoulin P., Rodriguez L., Janvier M.; Astronomy & Astrophysics (ISSN 0004-6361), Vol. 592, A118. [\[pdf\]](...) [\[code for reproduction\]](https://github.com/jimsrc/seatos)
* [_"Typical Profiles and Distributions of Plasma and Magnetic Field Parameters in Magnetic Clouds at 1 AU"_](https://link.springer.com/article/10.1007/s11207-016-0955-5), Rodriguez L., Masías-Meza J.J.; Dasso S.; Démoulin P., Zhukov A.N., Gulisano A., Mierla M., Kilpua E., West M., Lacatus D., Paraschiv A., Janvier M.; Solar Physics (ISSN 0038-0938), Vol. 291, Issue 7, pp.2145-2163. [\[pdf\]](...) [\[code for reproduction\]](https://github.com/jimsrc/seatos)
* [_"Solar Cycle Modulation of Cosmic Rays Observed with Low Energy Modes of the Pierre Auger Observatory"_](https://pos.sissa.it/236/074/pdf), Masías-Meza J.J. for the Pierre Auger Collaboration; Proc. 34th International Cosmic Ray Conference (ICRC), Volume PoS(ICRC), page 74 (ISSN 1824-8039). [\[code for reproduction\]](...)
* [_"Geomagnetic effects on cosmic ray propagation under different conditions for Buenos Aires and Marambio, Argentina"_](http://adsabs.harvard.edu/abs/2014SunGe...9...41M), Masías-Meza J.J. and Dasso S.; Sun and Geosphere, Vol.9, No.1, Pages 41-47, 2014, ISSN 1819-0839. [\[pdf\]](http://adsabs.harvard.edu/cgi-bin/nph-data_query?bibcode=2014SunGe...9...41M&link_type=ARTICLE&db_key=AST&high=)
* [_"Galactic cosmic ray decreases associated with non-interacting magnetic clouds in the 23rd solar cycle"_](https://www.cambridge.org/core/journals/proceedings-of-the-international-astronomical-union/article/galactic-cosmic-ray-decreases-associated-with-noninteracting-magnetic-clouds-in-the-23rd-solar-cycle/849594C5C002A960D8BB48A9510CA0E7), Masías-Meza J.J. and Dasso S.; Proc. of Nature of Prominences and their Role in Space Weather, IAU Symposium No. 300, (ISBN 9781107045194), 451-452, 2013. [\[pdf\]](https://www.cambridge.org/core/services/aop-cambridge-core/content/view/849594C5C002A960D8BB48A9510CA0E7/S1743921313011654a.pdf/galactic_cosmic_ray_decreases_associated_with_noninteracting_magnetic_clouds_in_the_23rd_solar_cycle.pdf)


<!--- EOF -->
