SHELL=/bin/bash

all:
	make clean
	make tags
	make build

rebuild:
	make clean
	make build

build:
	source ./funcs.sh && \
		echo -e "\033[0m" && \
		docker__jekyll_personal \
			-n jekyll_personal \
			-p 4000 \
			-d /work/web_pages/personal

tags:
	./generate_tag_pages.sh

clean:
	-rm -rf _site/*

#EOF
