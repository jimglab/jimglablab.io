---
layout: post
title: CUPS to print in the office
date:   2018-03-05 16:56
description: setup a CUPS client to print in the office
tags: docker CUPS
---

So I use Arch.
That's a boundary condition for setting up many things fast in a personal computer.
Of course this "boundary" is meant to be pushed forward.
Most of the time I can handle complicated scenarios, and sometimes not.
But lately I've taken the habit of setting my stuff in Docker containers.
I've come to think that it is the future of computing.
It promotes reproduction, isolation of workflows, testing frameworks, etc, which is what I like most.
So why don't I just change to another Linux ditro? Because I'm offered the choice to fine-grain-configure my box, it allows me to adventure into the Linux pipeline if I want to change something, and like to have the latest kernel and versions of certain applications.
Yes, that implies a little bit of masochism.

This time I needed to configure the CUPS service in order to print **something** in the printer of my office.
Up to two years ago, I had no problem because I managed with Ubuntu, so the config was much more handy.
I've had several "rounds" around this issue in my office but without success.
This latest round I took a Docker approach, and all went smooth...

The thing that discouraged me the most when trying to configure was the fact that I "needed" to get around some security details when configuring CUPS.
My box is configured around personalized security rules (in almost "paranoid" mode) that I develop whenever I have time.
The "zen" of my configuration is built around learning interesting things I find about the freedom of personalization that Linux allows us to have.

This is where Docker (again) has the perfect solution to me.
It offers the choice to use CUPS (an almost intrinsecally vulnerable application) from inside a ["honeypot"](https://en.wikipedia.org/wiki/Honeypot).
And this container (the "honeypot") desappears when it finishes sending the file to the printer.

This was my proof-of-concept test:

![image](../../images/cups_printed_yeiiii.png)

Of course, the commands were all executed from inside the container.
So now, everytime I need to print something I can automatically start a container that starts a CUPS client and sends some `.pdf` file I need to print.

The Dockerfile I wrote for this is [here](https://gitlab.com/jimglab/dockerfiles/tree/public/cups).
The command to start the container is in the README of that link.
If it gets to be useful for you too, let me know! :)


<!--- EOF -->
