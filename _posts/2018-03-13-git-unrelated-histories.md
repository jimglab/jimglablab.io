---
layout: post
title: Merge two unrelated Git repositories
date:   2018-03-13 17:02
description: We'll combine the histories of two unrelated Git repositories. I needed this to build my webpage.
tags: git workflow
---

Sometimes we want to bring the contents of a given repository `REPO2` (with remote `REMOTE2`, and branch `branch2`) to another repository, say `REPO1` (to local branch `branch1`).
These are the steps that always work for me:

{% highlight bash %}
# go to the branch where you'll "receive" the REPO2
git checkout branch1 && git clean -fd

# define the remote
git remote add <REMOTE2> <URL2>

# fetch it
git fetch <REMOTE2>

# merge && fix any conflicts by hand
git merge --no-commit --allow-unrelated-histories REMOTE2/branch2

# fix any conflicts && commit
git commit -m "merge w/ REMOTE2/branch2"
{% endhighlight %}

Done!

