---
layout: post
title: deployment of Tor hidden services
date:   2018-10-07 23:00
description: set up a hidden service open to the tor network
tags: docker tor hidden-service jekyll
---

Wouldn't it be nice to easily set up a TOR hidden service in your own computer, at home?
It turns out that the Tor community has [**made it very easy**](https://www.torproject.org/docs/tor-onion-service) for us to do that!
Of course, if you are paranoid about this, you can always make this go through a VPN connection, so your IP stays "anonymous".

The magic that surprised me is that after my first try with this, I could have an .onion address up and running! That is, it was already accessible from any Tor browser!
This is my first time seeing in live flesh the magic of the Tor network... it was also accessible by my friends with whom I showed off xD

So I tried to automate this by writing a `docker-compose.yml` and some bash scripts.
It's already tested, and I'm putting some effort to make it more friendly so I can share this joy.

In [this repository](https://gitlab.com/jimglab/tor-hidden-services) you can guide your self with the README to set up your own TOR hidden service.

In this case, I configured it to serve a webpage.
This repository contains two ways to do this: 
1) using a plain ASCII `index.html` file written by hand (see [here](https://gitlab.com/jimglab/tor-hidden-services/tree/jim/hidden-services/test)), and 
2) using Jekyll to generate static pages (see [here](https://gitlab.com/jimglab/tor-hidden-services/tree/jim/hidden-services/jekyll)).

It's needless to remark the importance of anonimity nowadays as a tool for people to practice their rights of freedom of speech (see [here1](https://cccure.training/m/articles/view/TOR-Project-Anonymity-Online), [here2](https://resources.infosecinstitute.com/hacking-tor-online-anonymity/), and, [here3](https://www.cigionline.org/publications/dark-web-dilemma-tor-anonymity-and-online-policing)).
So I'm happy to share some fun and tools to keep up with your rights.

<!--- EOF -->
