---
layout: post
title: Virtual network for pentesting with Docker :)
date:   2018-03-30 12:11
description: Set up a virtual network lab for pentesting
tags: docker kali network nmap
---

Hi!
So, I always wanted to set up a lab to practice/develop network skills using my laptop only.
Immediate solution is VirtualBox, but in order to be reproducible I would also need to work with [Vagrant](https://www.vagrantup.com/intro/index.html); but anyways it's all a heavy and "slow" process every time you need to test something fast.

So it just came to my slow mind: Docker! Of course! So stupid all this time it never occured to me.
So I googled a bit, and found some magazine (I would have to pay) and books about virtual network labs, but they all point to VirtualBox; and I just was in mood of playing with Docker and networks.

---
## Scan a container from my host
As no material was available on this, I investigated about nmap-scanning a Docker container from the host, and found a helpful [post](https://stackoverflow.com/questions/27937185/assign-static-ip-to-docker-container#35359185).
Really needed just two commands to: 1) set up a virtual network, and 2) start a container with a specific IP address in such network:
```bash
# start a virtual network
docker network create --subnet=172.19.0.0/24 --gateway 172.19.0.254 mynet123
# start some container within that network
docker run --rm -d --name markdown --net mynet123 --ip=172.19.0.5 jimsrc/markdown:v1
```
In this example I'm using a container that starts a web server that hosts a [Markdown editor](https://github.com/vifortech/docker-markdown-editor).
So now I can nmap-scan that `markdown` container because I know its IP address! Nice...

Let's see. So on my host I do:
```bash
sudo nmap 172.19.0.5 -sV -T3
[sudo] password for jim:

Starting Nmap 7.60 ( https://nmap.org ) at 2018-03-29 18:29 -03
Nmap scan report for 172.19.0.5
Host is up (0.000012s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    lighttpd 1.4.39
MAC Address: 02:42:AC:13:00:05 (Unknown)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 7.19 seconds
```

Ok.
So how do I know that `172.19.0.5` is some random public host on the internet I just scanned? :S
Sure there's some basic command to know that.
But we have a MAC address in the nmap results; so let's get into the `markdown` container and check!
```bash
$ docker exec -it markdown /bin/bash
/ # ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
32: eth0@if33: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:13:00:05 brd ff:ff:ff:ff:ff:ff
    inet 172.19.0.5/24 brd 172.19.0.255 scope global eth0
       valid_lft forever preferred_lft forever
```
You can see that the `link/ether` field of the interface `eth0@if33` has the **same** MAC address that the nmap results threw!
NICE :D



---
## Scan a container from a Kali container

This is nice, but I would like to practice pentesting stuff with a other network tools too (such as those included in a Kali distro).
I don't want to `sudo` from my host for each command that I practice, or install a whole plethora of tools into my system and keep track of its dependencies...
I can user Docker instead!

All I need is to start a [Kali container](https://hub.docker.com/r/kalilinux/kali-linux-docker/) within that `mynet123` virtual network.
So after pulling that container and [pulling wireless tools](https://gitlab.com/jimglab/dockerfiles/tree/public/kali/wireless), you could scan for services on the whole virtual network:

```bash
# nmap 172.19.0.0/24 -sV -T5

Starting Nmap 7.60 ( https://nmap.org ) at 2018-03-30 15:32 UTC
Nmap scan report for markdown.mynet123 (172.19.0.5)
Host is up (0.000028s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    lighttpd 1.4.39
MAC Address: 02:42:AC:13:00:05 (Unknown)

Nmap scan report for 172.19.0.254
Host is up (0.000021s latency).
All 1000 scanned ports on 172.19.0.254 are closed
MAC Address: 02:42:E7:9E:AE:49 (Unknown)

Nmap scan report for 67d0d9dd65ea (172.19.0.1)
Host is up (0.0000090s latency).
All 1000 scanned ports on 67d0d9dd65ea (172.19.0.1) are closed

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 256 IP addresses (3 hosts up) scanned in 8.35 seconds
```
Note that we are using `-T5` option ("insane" mode) to accelerate the scan (which we wouldn't use in real world because it's "noisy").

So we have 3 host detected up by NMap.
The first one is the `markdown` server, the second is the gateway of the network (the virtual router?), and the third is Kali container itself.

---
Finally, just to say that we've managed a way to setup a virtual net in the "Docker way".
Instead of the `markdown` server example, we could test penetrations to any service we setup in a container.
Such "service" could be a: web-service, Dropbox traffic, FTP/SSH server, WordPress, CUPS, ... (well, get creative), and apply known exploits.
Even OpenVPN traffic could be analyzed and perform a research for bugs.
[Here](https://github.com/enaqx/awesome-pentest) are some related Docker toys to complement.
Happy long nights! :)


<!--- EOF -->
