---
layout: post
title: Run Mendeley under Docker
date:   2017-12-01 14:07
description: Mendeley under Docker.
tags: workflow docker mendeley papers
---

Everytime I need to remember which paper said what, I have to search the contents of my collection of papers.
Even though I'd love a terminal application for this, there's none on the internet.
So my current solution is [Mendeley](https://www.mendeley.com); which is pretty friendly.
If it had a "dark" theme for my eyes, that would be awesome! but anyway.

So this is an app that I wouldn't like to fight for in terms of maintaning its dependencies or watch for any conflicts of them.
It has not interest on me concerning security issues since I can use it offline.
However, it's a must for any research I want to carry on.

So I "Dockerized" it :)
This is my [Dockerfile](https://gitlab.com/jimglab/dockerfiles/blob/public/mendeley/Dockerfile).

I can use it with whatever directory of `.pdf` files.
Here is a piece of a bash function that I use to start an instance of that Docker image:
```bash
docker run --name $CNAME \
    --rm \
    -d \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -e DISPLAY=unix$DISPLAY \
    --net=none \
    --user=$UID:$GID \
    -v ${DIR_PAPERS}:/papers \
    -v ${DIR_HOME}:/home/docker \
    ${DockerImage} \
    mendeleydesktop
```
Note that:
* I start this container with no access to the world; which prevents it from doing it's stupid syncronization everytime it starts up.
* the `DIR_PAPERS` path is arbitrary and I can choose it with a CLI argument of my bash script.
* the `DIR_HOME` is to preserve the databse of metadata that Mendeley collects for a given set of `.pdf` files in the `DIR_PAPERS` directory. So this actually is a path for "persistent" metadata I hold on my host for every `DIR_PAPERS` path I use. For example, I often use Mendeley separately for scientific research, and for papers about Linux and stuff.


<!--- EOF -->
