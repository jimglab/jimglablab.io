#!/bin/bash

function docker__jekyll_personal(){
    #--- dirs 
    local GirRepo_HOST

    #--- ports (defaults)
    local PHOST=4000    # port in host
    local PGUST=4000    # port in guest

    #--- etc
    local CNAME="jekyll_personal"   # container name

    #--- grab args
    [[ $# -eq 0 ]] && {
        # TODO: print help.
        echo -e "\n [-] we need arguments! XD\n";
        return 1;
    }
    while [[ $# -gt 0 ]]; do
        local key="$1"
        case $key in
            -d)
            GitRepo_HOST="$2"       # reset only if explicitly said
            shift; shift
            ;;
            -p)
            PHOST=$2                # port in host
            shift; shift
            ;;
            -n)
            CNAME="$2"              # container name
            shift; shift
            ;;
            *)
            echo " [-] unknown argument!"
            return 1
        esac
    done

    echo -e "\n [*] Starting Jekyll using repo:\n     ${GitRepo_HOST}\n"

    docker run --rm -it \
        --name $CNAME \
        -p 127.0.0.1:$PHOST:$PGUST \
        -v ${GitRepo_HOST}:/srv/jekyll \
        jekyll/jekyll:3.5 \
        jekyll serve --watch --force_polling --trace --port $PGUST --incremental
    # NOTE: it doesn't update changes on the fly. The --force_polling doesn't help.
    # See:
    # https://stackoverflow.com/questions/45088475/jekyll-sees-changes-in-css-doesnt-regenerate-em
}

